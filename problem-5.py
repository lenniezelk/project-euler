'''
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
'''


def check_divisible():
    num = 2521
    divisors = list(range(11, 21))
    while True:
        for i in divisors:
            r = num % i
            if r:
                num += 1
                break
        else:
            return num


if __name__ == '__main__':
    print(check_divisible())
