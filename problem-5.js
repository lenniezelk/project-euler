const _ = require('lodash');


function checkDivisible() {
  let num = 2521;
  const divisors = _.range(11, 21);
  while (true) {
    let allDivisible = true;
    for (const x of divisors) {
      const mod = num % x;
      if (mod !== 0) {
        num += 1;
        allDivisible = false;
        break;
      }
    }
    if (allDivisible) {
      return num;
    }
  }
}


console.log(checkDivisible());
