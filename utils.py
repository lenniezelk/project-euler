import math


def check_prime(N, start=2):
    val = math.ceil(math.sqrt(N))
    for i in range(start, val + 1):
        if N % i == 0:
            return False
    return True
