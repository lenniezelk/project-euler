/*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
*/


const utils = require('./utils.js');


function countPrimes() {
  let num = 14;
  const divisors = [2, 3, 5, 7, 11, 13];
  let count = 6;

  while (true) {
    let isDivisible = false;
    for (const d of divisors) {
      if (utils.isDivisibleBy(num, d)) {
        isDivisible = true;
        break;
      }
    }
    if (!isDivisible) {
      if (utils.checkPrime(num)) {
        count += 1;
      }
      if (count === 10001) {
        break;
      }
    }
    num += 1;
  }

  return num;
}

if (require.main === module) {
  console.log(countPrimes());
}
