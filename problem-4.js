const _ = require('lodash');


function checkPalindrome(num) {
  const numStr = num.toString();
  const numArray = numStr.split('').reverse();
  return numStr === numArray.join('');
}

function multiplyNumbers(lower, upper) {
  const palindromes = [];
  const numbers = _.range(lower, upper);
  for (const x of numbers) {
    for (const y of numbers) {
      const res = x * y;
      if (checkPalindrome(res)) {
        palindromes.push(res);
      }
    }
  }
  return Math.max(...palindromes);
}

console.log(multiplyNumbers(900, 1000));
