const _ = require('lodash');

function checkPrime(N, start = 2) {
  const val = Math.ceil(Math.sqrt(N));
  for (const i of _.range(start, val + 1)) {
    if (N % i === 0) {
      return false;
    }
  }
  return true;
}

function isDivisibleBy(quot, divisor) {
  return quot % divisor === 0;
}

module.exports.checkPrime = checkPrime;
module.exports.isDivisibleBy = isDivisibleBy;
