# Project Euler solutions #

Solutions to Project Euler problems.

### How to run

* For the `Python` solutions: `python filename`, Python 3 in use.
* To check formatting etc for python run `flake8`
* For the `JavaScript` solutions:
	Ensure you have `yarn` installed
	Install project deps: `yarn install`
	`node filename` to run a solution.
* To check formatting etc for JS run `./node_modules/.bin/eslint .`
