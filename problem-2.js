/*
Euler problem 2

Each new term in the Fibonacci sequence is generated by adding the previous two terms.
By starting with 1 and 2, the first 10 terms will be:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

By considering the terms in the Fibonacci sequence whose values do not exceed four million,
find the sum of the even-valued terms.
*/


function Memoize(f) {
  this.f = f;
  this.memo = {};
  this.c = function (args) {
    if (!(args in this.memo)) {
      this.memo[args] = this.f.call(null, args);
    }
    return this.memo[args];
  };
}

function fibonacci(n) {
  if (n < 2) {
    return n;
  }
  return fibonacci(n - 1) + fibonacci(n - 2);
}

const fibonacciM = new Memoize(fibonacci);

function solution() {
  let total = 0;
  for (let i = 0; i < 35; i + 1) {
    const value = fibonacciM.c(i);
    if (value % 2 === 0) {
      total += value;
    }
  }
  return total;
}

console.log(solution());
