"""
Euler Project Problem 3

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""

import math
import multiprocessing
from concurrent.futures import ProcessPoolExecutor


def compute_factors(values, N):
    for i in range(values[0], values[1] + 1):
        if N % i == 0:
            yield i


def check_prime(N):
    val = math.ceil(math.sqrt(N))
    for i in range(2, val + 1):
        if N % i == 0:
            return False
    return True


def get_prime_factors(values, N=600851475143):
    return list(filter(
        check_prime, compute_factors(values, N)))


if __name__ == '__main__':
    pool = ProcessPoolExecutor(max_workers=multiprocessing.cpu_count())
    res = pool.map(
        get_prime_factors, (
            (1, 100000000000),
            (100000000001, 200000000000),
            (200000000001, 300000000000),
            (300000000001, 400000000000),
            (400000000001, 500000000000),
            (500000000001, 600000000000),
            (600000000001, 600851475143),
        ))
    print(max(res))
