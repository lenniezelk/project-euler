const _ = require('lodash');


function sumSquares() {
  const nums = _.range(1, 101);
  const sqSums = _.sum(nums) ** 2;
  const sumSq = _.sum(nums.map(x => x ** 2));
  return sqSums - sumSq;
}

console.log(sumSquares());
