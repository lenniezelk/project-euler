'''
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
'''


def check_palindrome(num):
    num_str = str(num)
    return num_str == ''.join(reversed(num_str))


def multiply_numbers(lower, upper):
    nums = list(range(lower, upper))
    palindromes = []
    for x in nums:
        for y in nums:
            res = x * y
            if check_palindrome(res):
                palindromes.append(res)
    return max(palindromes)


if __name__ == '__main__':
    print(multiply_numbers(900, 1000))
