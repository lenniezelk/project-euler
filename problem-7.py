'''
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
'''


from utils import check_prime


def is_divisible_by(num, divisor):
    return num % divisor == 0


def count_primes():
    num = 14
    divisiors = (2, 3, 5, 7, 11, 13)
    count = 6
    while True:
        for d in divisiors:
            if is_divisible_by(num, d):
                break
        else:
            if check_prime(num):
                count += 1
            if count == 10001:
                break
        num += 1
    return num


if __name__ == '__main__':
    print(count_primes())
